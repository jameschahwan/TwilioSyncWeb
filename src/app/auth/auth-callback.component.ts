import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from './../../environments/environment';
import { Http, Headers, RequestOptions } from '@angular/http';
import { AuthHttp } from 'angular2-jwt';
import { AuthService } from './auth.store'
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'
@Component({
    selector: 'auth-callback',
    templateUrl: `./auth-callback.html`
})

export class AuthCallbackComponent implements OnInit {
    qParams: any;
    constructor(private activateRoute: ActivatedRoute, private router: Router, private http: Http, private store: AuthService) {

    }

    ngOnInit() {
        this.activateRoute.params.subscribe(params => {
            this.qParams = this.activateRoute.snapshot.queryParams;
            if (!this.qParams["error"]) {
                if (this.qParams["code"] != null) {
                    let body = {
                        code: this.qParams["code"],
                        status: ''
                    }

                    let headers = new Headers({ 'Content-Type': 'application/json' });
                    let options = new RequestOptions({ headers: headers });
                    this.http.post(environment.apiUrl + '/servicem8/connect',
                        JSON.stringify(body), options)
                        .map(res => res.json())
                        .subscribe(data => {
                            console.log("Saving data " + data.toString())
                            this.store.dispatchVendorUUID(data.toString());
                            console.log("Navigating")
                            this.router.navigate(['twilio-registration']);
                        });


                }
            }
            else {

            }
        })
    }
}

                    // let headers = new Headers({ 'Content-Type': 'application/json' });
                    // let options = new RequestOptions({ headers: headers });
                    // var promise = this.http.post(environment.apiUrl + '/servicem8/connect',
                    //     JSON.stringify(body), options)
                    //     .subscribe(data => {
                    //         // console.log("Saving data " + data)
                    //         // this.store.saveJobCategories(data);
                    //         // this.router.navigate(['category-selection']);
                    //     });