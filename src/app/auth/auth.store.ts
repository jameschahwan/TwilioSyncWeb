import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'
import { environment } from './../../environments/environment';
import { Http, Headers, RequestOptions } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';

export const vendorUUID = (state: any = [], { type, payload }) => {
    switch (type) {
        case 'SAVE_VENDOR':
            return payload;
        default:
            return state;
    }
}

export const vendorName = (state: any = {}, { type, payload }) => {
    switch (type) {
        case 'SAVE_VENDOR_NAME':
            return payload;
        default:
            return state;
    }
}

export const categoriesServiceM8 = (state: any = [], { type, payload }) => {
    switch (type) {
        case 'SAVE_CATEGORIES_SERVICEM8':
            return payload;
        default:
            return state;
    }
}

export const numbersTwilio = (state: any = [], { type, payload }) => {
    switch (type) {
        case 'SAVE_TWILIO_NUMBER':
            return payload;
        default:
            return state;
    }
}

export interface AuthStore {
    vendorUUID: any;
    categoriesServiceM8: any;
    numbersTwilio: any;
    vendorName: any
}

@Injectable()
export class AuthService {
    vendorUUID: Observable<any>;
    vendorUUIDSnapshot: any;
    categoriesServiceM8: Observable<any>;
    categoriesServiceM8Snapshot: any;
    numbersTwilio: Observable<any>;
    numbersTwilioSnapshot: any;
    vendorName: Observable<any>;
    vendorNameSnapshot: any;

    constructor(private store: Store<AuthStore>, private http: Http, private router: Router) {
        this.vendorUUID = this.store.select<any>('vendorUUID');
        this.vendorUUID.subscribe(s => this.vendorUUIDSnapshot = s);

        this.categoriesServiceM8 = this.store.select<any>('categoriesServiceM8');
        this.categoriesServiceM8.subscribe(s => this.categoriesServiceM8Snapshot = s);

        this.numbersTwilio = this.store.select<any>('numbersTwilio');
        this.numbersTwilio.subscribe(s => this.numbersTwilioSnapshot = s);

        this.vendorName = this.store.select<any>('vendorName');
        this.vendorName.subscribe(s => this.vendorNameSnapshot = s);
    }

    dispatchVendorUUID(vendor: any) {
        console.log("Saving: " + vendor)
        this.store.dispatch({ type: 'SAVE_VENDOR', payload: vendor })
        console.log("Testing " + this.vendorUUIDSnapshot)
    }

    registerTwilio(identifier: any, twilioSID: any, twilioAuthToken: any) {

        let body = {
            vendor_id: identifier,
            auth_token: twilioAuthToken,
            sid: twilioSID
        }
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        this.http.post(environment.apiUrl + '/twilio/register',
            body, options)
            .map(res => res.json())
            .subscribe(data => {

                //need to save categories and available numbers to store here
                console.log("DATA: " + JSON.stringify(data));
                console.log("ATTEMPTING TO STORE CATEGORIES: " + data.categories)
                this.store.dispatch({ type: 'SAVE_CATEGORIES_SERVICEM8', payload: data.categories });
                console.log(this.categoriesServiceM8Snapshot)

                console.log("ATTEMPTING TO STORE TWILIO NUMBERS: " + data.numbers)
                this.store.dispatch({ type: 'SAVE_TWILIO_NUMBER', payload: data.numbers });
                console.log(this.numbersTwilioSnapshot);
                this.store.dispatch({ type: 'SAVE_VENDOR_NAME', payload: data.vendor_name });

                this.router.navigate(['twilio-messaging-options']);
            });
    }

    saveTwilioOptions(identifier: string, selectedTwilioNumber: string, jobCreation: boolean, selectedJobCategory: string, jobCreationReply: boolean, creationReplyText: string, statusReply: boolean, scheduleReply: boolean) {

        let body = {
            vendor_id: identifier,
            selectedTwilioNumber: selectedTwilioNumber,
            jobCreation: jobCreation,
            selectedJobCategory: selectedJobCategory,
            jobCreationReply: jobCreationReply,
            creationReplyText: creationReplyText,
            statusReply: statusReply,
            scheduleReply: scheduleReply
        }
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        this.http.post(environment.apiUrl + '/twilio/options',
            body, options)
            .map(res => res)
            .subscribe(data => {

                this.router.navigate(['auth-success']);
            });
    }

}