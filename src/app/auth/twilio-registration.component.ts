import { Component, OnInit, OnChanges } from '@angular/core';
import { AuthService } from './auth.store'
import { FormBuilder, FormControl, FormGroup, Validators, FormArray } from '@angular/forms'
@Component({
    selector: 'twilio-registration',
    templateUrl: './twilio-registration.html',
})

export class TwilioRegistrationComponent implements OnInit {
    twoInputs = {SidToken: '', AuthToken:''};
    identifier: any = "";
    twilioForm: FormGroup;
    constructor(private authService: AuthService, private fb: FormBuilder) { }

    ngOnInit() {

        this.authService.vendorUUID.subscribe(s => this.identifier = s);
        this.twilioForm = new FormGroup({
            'SidToken': new FormControl(this.twoInputs.SidToken, [
                Validators.required,
                Validators.minLength(34),
                 Validators.pattern("A{1}C{1}[A-Za-z0-9]{32}")]),
            'AuthToken':new FormControl(this.twoInputs.AuthToken, [
                Validators.required,
                Validators.minLength(32)])}
        );
    }
    get SidToken() { return this.twilioForm.get('SidToken'); }
    
    get AuthToken() { return this.twilioForm.get('AuthToken'); }
    register() {

        console.log(this.twilioForm.get('SidToken').value);
        console.log(this.twilioForm.get('AuthToken').value);
        //don't need the bool error checks, since the form self validates
            this.authService.registerTwilio(this.identifier, this.twilioForm.get('SidToken').value, this.twilioForm.get('AuthToken').value)
     
    }
}

class CategoryObject {
    name: string;
    selected: boolean;
}





