import { Component, OnInit } from '@angular/core';
import { AuthService } from './auth.store';
@Component({
    selector: 'twilio-messaging-options',
    templateUrl: './twilio-messaging-options.html'
})

export class TwilioMessagingOptionsComponent implements OnInit {
    jobCreationReplyError: boolean = false;
    numberSelectionError: boolean = false;
    categorySelectionError: boolean = false;


    selectedTwilioNumber: string;
    selectedJobCategory: string;
    creationReplyText: string;
    identifier: string = "";

    twilioNumbers: any[];
    servicem8Categories: any[];

    vendorName: string = "";
    default_message: string = "Auto-reply from " + this.vendorName + ": We have processed your message. Job ID is: [JOB_ID].";

    jobCreationReply: boolean = false;
    jobCreation: boolean = true; // for now as there is only one thing to do atm

    statusReply: boolean = false;
    scheduleReply: boolean = false;

    constructor(private authService: AuthService) { }

    ngOnInit() {
        this.authService.vendorUUID.subscribe(s => this.identifier = s);
        this.authService.categoriesServiceM8.subscribe(s => this.servicem8Categories = s);
        this.authService.numbersTwilio.subscribe(s => this.twilioNumbers = s);
        this.authService.vendorName.subscribe(s => this.vendorName = s);
    }

    jobCreationReplySelect() {
        this.jobCreationReply = !this.jobCreationReply;
    }

    saveOptions() {

        if (this.noErrors()) {
            this.authService.saveTwilioOptions(this.identifier, this.selectedTwilioNumber, this.jobCreation, this.selectedJobCategory, this.jobCreationReply, this.creationReplyText, this.statusReply, this.scheduleReply);
        }
    }

    jobStatusReplySelect() {
        this.statusReply = !this.statusReply;
        this.setDefaultMessage()
    }

    jobScheduleReplySelect() {
        this.scheduleReply = !this.scheduleReply;
        this.setDefaultMessage();
    }

    setDefaultMessage() {
        this.default_message = "Auto-reply from " + this.vendorName + ": We have processed your message. Job ID is: [JOB_ID]."

        if (this.statusReply == true && this.scheduleReply == true) {
            this.default_message = this.default_message + " Text !STATUS or !WHEN followed by your job id for a status update or appointment date"
        }
        else if (this.statusReply == true && this.scheduleReply == false) {
            this.default_message = this.default_message + " Text !STATUS followed by your job id for a status update"
        }
        else if (this.statusReply == false && this.scheduleReply == true) {
            this.default_message = this.default_message + " Text !WHEN followed by your job id for your appointment date"
        }
    }

    noErrors() {

        if (this.selectedTwilioNumber == "" || this.selectedTwilioNumber == null) {
            this.numberSelectionError = true;
        }
        else {
            this.numberSelectionError = false;
        }

        if (this.selectedJobCategory == "" || this.selectedJobCategory == null) {
            this.categorySelectionError = true;
        }
        else {
            this.categorySelectionError = false;
        }

        if (this.jobCreationReply == true) {
            if (this.creationReplyText == "" || this.creationReplyText == null) {
                this.jobCreationReplyError = true;
            }
            else {
                this.jobCreationReplyError = false;
            }
        }
        else {
            this.jobCreationReplyError = false;
        }


        if (this.categorySelectionError == false && this.numberSelectionError == false && this.jobCreationReplyError == false) {
            return true;
        }
        else {
            return false;
        }
    }
}