import { Component, OnInit } from '@angular/core';
import { environment } from './../../environments/environment';

@Component({
    selector: 'auth-initiate',
    templateUrl: './auth-initiate.html'
})

export class AuthInitiateComponent implements OnInit {
    constructor() { }

    ngOnInit() { }

    onClick() {
        //let scopes = 'read_jobs manage_tasks vendor vendor_logo vendor_email publish_email read_customer_contacts read_job_contacts read_customers read_staff publish_sms';
        let scopes = 'vendor manage_jobs vendor_email';
        let params = new URLSearchParams();
        params.set('scope', scopes.toString()); // the user's search value
        params.set('redirect_uri', environment.hostUrl + '/callback');
        params.set('response_type', 'code');
        var clientid = '790742';
        if (environment.production) {
            clientid = "618030";
        }

        params.set('client_id', clientid);
        location.href = 'https://www.servicem8.com/oauth/authorize?' + params.toString();
    }
}