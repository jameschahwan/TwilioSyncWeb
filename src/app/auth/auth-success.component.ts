import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'auth-success',
    templateUrl: './auth-success.html'
})

export class AuthSuccessComponent implements OnInit {
    constructor() { }

    ngOnInit() { }

    onClick() {
        location.href = 'https://go.servicem8.com'
    }
}