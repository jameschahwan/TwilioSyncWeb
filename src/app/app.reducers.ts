import { ActionReducer, combineReducers } from '@ngrx/store';
import { environment } from './../environments/environment';
import { vendorUUID, categoriesServiceM8, numbersTwilio, vendorName } from './auth/auth.store'
const reducers = {
    vendorUUID, categoriesServiceM8, numbersTwilio, vendorName
};

const developmentReducer: ActionReducer<any> = combineReducers(reducers);
const productionReducer: ActionReducer<any> = combineReducers(reducers);

export function reducer(state: any, action: any) {
    if (environment.production) {
        return productionReducer(state, action);
    } else {
        return developmentReducer(state, action);
    }
}

