import { Routes, RouterModule } from '@angular/router';
import { AuthInitiateComponent } from './auth/auth-initiate.component';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { AuthCallbackComponent } from './auth/auth-callback.component'
import { TwilioRegistrationComponent } from './auth/twilio-registration.component'
import { TwilioMessagingOptionsComponent } from './auth/twilio-messaging-options.component';
import { AuthSuccessComponent } from './auth/auth-success.component'
const appRoutes: Routes = [

    { path: 'initiate', component: AuthInitiateComponent },
    { path: 'callback', component: AuthCallbackComponent },
    { path: 'twilio-registration', component: TwilioRegistrationComponent },
    { path: 'twilio-messaging-options', component: TwilioMessagingOptionsComponent },
    { path: 'auth-success', component: AuthSuccessComponent },
    {
        path: '',
        redirectTo: 'initiate',
        pathMatch: 'full',
    },

];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
