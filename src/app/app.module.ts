import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthInitiateComponent } from './auth/auth-initiate.component'
import { AuthCallbackComponent } from './auth/auth-callback.component'
import { TwilioRegistrationComponent } from './auth/twilio-registration.component'
import { AuthSuccessComponent } from './auth/auth-success.component'
import { TwilioMessagingOptionsComponent } from './auth/twilio-messaging-options.component';
import { AppComponent } from './app.component';
import { Store, StoreModule } from '@ngrx/store';
import { reducer } from './app.reducers';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule, MatCheckboxModule, MatRadioModule, MatProgressSpinnerModule, MatSelectModule, MatInputModule, MAT_PLACEHOLDER_GLOBAL_OPTIONS, MatCardModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { routing } from './app.routing';
import { provideAuth, AuthHttp, AuthConfig } from 'angular2-jwt';
import { HttpModule, Http, RequestOptions } from '@angular/http';
import { AuthService } from './auth/auth.store'
export function authHttpServiceFactory(http: Http, options: RequestOptions) {
  return new AuthHttp(new AuthConfig({}), http, options);
}

@NgModule({

  imports: [
    routing,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    BrowserAnimationsModule,
    StoreModule.provideStore(reducer),
    FlexLayoutModule,
    MatButtonModule, MatCheckboxModule, MatCardModule, MatInputModule, MatProgressSpinnerModule, MatRadioModule, MatSelectModule

  ],
  declarations: [
    AppComponent,
    AuthInitiateComponent,
    AuthCallbackComponent,
    TwilioRegistrationComponent,
    AuthSuccessComponent, TwilioMessagingOptionsComponent
  ],
  providers: [
    {
      provide: AuthHttp,
      useFactory: authHttpServiceFactory,
      deps: [Http, RequestOptions]
    }, AuthService,
    { provide: MAT_PLACEHOLDER_GLOBAL_OPTIONS, useValue: { float: 'always' } }
  ],
  exports: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
