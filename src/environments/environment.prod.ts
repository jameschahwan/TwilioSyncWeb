export const environment = {
  production: true,
  apiUrl: 'https://servicem8twiliosyncapi.azurewebsites.net/api',
  hostUrl: 'https://servicem8twiliosync.azurewebsites.net',
  serverUrl: 'https://servicem8twiliosyncapi.azurewebsites.net/'


  // apiUrl: 'http://advancednotificationapi.azurewebsites.net/api',
  // hostUrl: 'http://advancednotification.azurewebsites.net',
  // serverUrl: 'http://advancednotificationapi.azurewebsites.net/'
};
