import { AdvancedNotificationUIPage } from './app.po';

describe('advanced-notification-ui App', () => {
  let page: AdvancedNotificationUIPage;

  beforeEach(() => {
    page = new AdvancedNotificationUIPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
